import React from 'react'
import { Route, Routes } from 'react-router-dom'
import ProductForm from './productForm/ProductForm'
import Products from './productForm/Products'

const MyRoutes = () => {
  return (
    <div>
        <Routes>
            <Route path='/products/create' element={<ProductForm></ProductForm>}></Route>

            
            <Route path='/products' element={<Products></Products>}></Route>
        </Routes>
    </div>
  )
}

export default MyRoutes