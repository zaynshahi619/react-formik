import React from 'react'
import { Outlet, Route, Routes } from 'react-router-dom'

const Nesting = () => {
    //localhost:3000/
    //localhost:3000/create-products/
    //localhost:3000/create-products/view-products/
    //localhost:3000/create-products/view-products/update-products
  return (
    <div>
        <Routes>
            <Route path="/" element={<div><Outlet></Outlet></div>}>
                <Route index element={<div>Home Page</div>}></Route>
                <Route path="create-products" element={<div><Outlet></Outlet></div>}>
                    <Route index element={<div>Create product page</div>}></Route>
                    <Route path='view-products' element={<div><Outlet></Outlet></div>}>
                        <Route index element={<div>View products page</div>}></Route>
                        <Route path='update-products' element={<div><Outlet></Outlet></div>}>
                            <Route index element={<div>Update products page</div>}></Route>
                        </Route>
                        
                    </Route>
                </Route>
            </Route>
        </Routes>
    </div>
  )
}

export default Nesting