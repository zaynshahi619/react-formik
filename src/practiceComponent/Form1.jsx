import React, { useState } from 'react'

const Form1 = () => {
    let [name,setName] = useState("")
    let [lastName,setLastName] = useState("")
    let [email,setEmail] = useState("")
    let [date,setDate] = useState("")
    let [isMarried,setIsMarried] = useState(false)
    let [day,setDay] = useState("")
    let [gender,setGender] = useState("male")

    let days = [
      {
        label: "Sunday",
        value: "day1",
      },
      {
        label: "Monday",
        value: "day2",
      },
      {
        label: "Tuesday",
        value: "day3",
      },
      {
        label: "Wednesday",
        value: "day4",
      },
      {
        label: "Thursday",
        value: "day5",
      },
      {
        label: "Friday",
        value: "day6",
      },
      {
        label: "Saturday",
        value: "day7",
      },
    ];

    let onSubmit = (e)=>{
        e.preventDefault()
        let data = {
          name:name,
          lastName:lastName,
          email:email,
          date:date,
          isMarried:isMarried,
          day:day,
          gender:gender
          
        }
        console.log(data);
    }
  return (
    <div>
        <form onSubmit={onSubmit}>
          <div>
          <label htmlFor='name'>Name:</label>
            <input type='text' placeholder='eg.Ram' value={name} onChange={(e)=>{setName(e.target.value)}} id='name'></input>
          </div>
          <div>
            <label htmlFor='lastName'>LastName:</label>
            <input type='text' placeholder='lastname' value={lastName} onChange={(e)=>{setLastName(e.target.value)}} id='lastName'></input>

          </div>
          <div>
            <label htmlFor='email'>Email:</label>
            <input type='email' placeholder='eg.abc@gmail.com' id='email' value={email} onChange={(e)=>{setEmail(e.target.value)}}></input>
          </div>
          <div>
            <label htmlFor='date'>Date:</label>
            <input type='date' id='date' value={date} onChange={(e)=>{setDate(e.target.value)}}></input>
          </div>
          <div>
            <label htmlFor='isMarried'>isMarried:</label>
            <input type='checkbox' id='isMarried' checked={isMarried ===true} onChange={(e)=>{setIsMarried(e.target.checked)}}></input>
          </div>

          <div>
            <label>Day:</label>
            <select value={day} onChange={(e)=>{setDay(e.target.value)}}>
              {days.map((items,i)=>{
                return(<option value={items.value}>{items.label}</option>)
              })}
            </select>
          </div>

          <div>
            <label>Gender:</label>
            <label htmlFor='male'>Male</label>
            <input type='radio' id='male' value="male" checked={gender === 'male'} onChange={(e)=>{setGender(e.target.value)}}></input>
            <label htmlFor='female'>Female</label>
            <input type='radio' id="female" value="female" checked={gender === 'female'} onChange={(e)=>{setGender(e.target.value)}}></input>
            <label htmlFor='others'>Others:</label>
            <input type='radio' id='others' value="others" checked={gender === "others"} onChange={(e)=>{setGender(e.target.value)}}></input>
          </div>

          <button type='submit'>Proceed</button>
            

        </form>
        
    </div>
  )
}

export default Form1