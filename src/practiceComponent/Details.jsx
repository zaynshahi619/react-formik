import React from 'react'

const Details = (props) => {
  return (
    <div>
        Name is {props.name}<br></br>
        Age is {props.age}<br></br>
        Address is {props.address}
    </div>
  )
}

export default Details