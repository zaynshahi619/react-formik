import React, { useEffect, useState } from 'react'

const LearnUseEffect = () => {
    let [count1,setCount1] = useState(0)
    let [count2,setCount2] = useState(100)

    useEffect(()=>{
      console.log("i am useEffect");
    },[count1])
    console.log("i am");
   
  return (
    <div>
      {count1}
      <br></br>
      <button onClick={(e)=>{
        setCount1(count1+1)
      }}>Increment</button>
      <br></br>
      {count2}
      <br></br>
      <button onClick={(e)=>{
        setCount2(count2+1)
      }}>Increment</button>
    </div>
  )
}

export default LearnUseEffect