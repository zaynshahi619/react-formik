import React from 'react'
import { NavLink } from 'react-router-dom'

const MyLinks = () => {
  return (
    <div>
        <NavLink to="http://localhost:3000/products/create" style={{padding:"15px"}}>
            Create product
        </NavLink>
        <NavLink to="http://localhost:3000/products" style={{padding:"15px"}}>
            Product
        </NavLink>
    </div>
  )
}

export default MyLinks