import React, { useState } from 'react'

const LearnUseState = () => {
    let [name,setName] = useState("ram")
    let [age,setAge] = useState(25)

    let handleName = (e)=>{
        setName("bikram")
    }
    let handleAge = (e)=>{
        setAge(30)
    }
  return (
    <div>
        <p>My name is {name}</p>
        <button onClick={handleName}>Change Name</button>
        <p>My age is {age}</p>
        <button onClick={handleAge}>Change Age</button>
        
    </div>
  )
}

export default LearnUseState