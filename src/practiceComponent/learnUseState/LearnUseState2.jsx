import React, { useState } from 'react'

const LearnUseState2 = () => {
    let [showImage,setShowImage] = useState(true)
    let handleShow = (isDisplay)=>{
        return (e)=>{
            setShowImage(isDisplay)
        }
    }
  return (
    <div>{showImage?<img src='./logo192.png'></img>:null}
    <button onClick={handleShow(true)}>Show Image</button>
    <button onClick={handleShow(false)}>Hide Image</button>
    </div>

  )
}

export default LearnUseState2