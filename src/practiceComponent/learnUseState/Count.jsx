import React, { useState } from 'react'

const Count = () => {
    let [count,setCount] = useState(0)
    let countUp = (e)=>{
        if(count<10){
            setCount(count+1)
        }
    }
    let countDown = (e)=>{
        if(count>0){
        setCount(count-1)

        }
    }
    let reset = (e)=>{
        setCount(0)
    }
  return (
    <div>
        {count}
        <br></br>
        <button onClick={countUp}>Up</button>
        <button onClick={countDown}>Down</button>
        <button onClick={reset}>Reset</button>
        </div>
  )
}

export default Count