import React, { useState } from 'react'

const Toggle = () => {
    let [showImage,setShowImage] = useState(true)
    let handleClick = (e)=>{
        if(showImage){
            setShowImage(false)
        }
        else{
            setShowImage(true)
        }
    }

  return (
    <div>
        {showImage?<img src='./logo192.png'></img>:null}
        <button onClick={handleClick}>{showImage?"hide":"show"}</button>
    </div>
  )
}

export default Toggle