import React, { useState } from 'react'

const ImageHideShow = () => {
    let [showImage,setShowImage] = useState(true)

    let hideImageFunc = (e)=>{
        setShowImage(false)
    }
    let showImageFunc = (e)=>{
        setShowImage(true)
    }
  return (
    <div>
        {showImage?<img src='./logo192.png'></img>:null}
        <button onClick={hideImageFunc}>Hide</button>
        <button onClick={showImageFunc}>Show</button>
    </div>
  )
}

export default ImageHideShow