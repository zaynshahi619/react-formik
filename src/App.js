import { startTransition, useState } from "react";
import LearnUseEffect from "./practiceComponent/LearnUseEffect/LearnUseEffect";
import MapPractice from "./practiceComponent/MapPractice";
import Count from "./practiceComponent/learnUseState/Count";
import ImageHideShow from "./practiceComponent/learnUseState/ImageHideShow";
import LearnUseState from "./practiceComponent/learnUseState/LearnUseState";
import LearnUseState2 from "./practiceComponent/learnUseState/LearnUseState2";
import Toggle from "./practiceComponent/learnUseState/Toggle";
import AssignmentTernary from "./practiceComponent/learnUseState/AssignmentTernary";
import Details from "./practiceComponent/Details";
import Info from "./practiceComponent/Info";
import LearnCleanUpFun from "./practiceComponent/LearnUseEffect/LearnCleanUpFun";
import MyLinks from "./practiceComponent/MyLinks";
import MyRoutes from "./practiceComponent/MyRoutes";
import Nesting from "./practiceComponent/Nesting";
import Form1 from "./practiceComponent/Form1";
import FormikForm from "./Formik/FormikForm";
import FormikTutorial from "./Formik/FormikTutorial";

function App() {
  // let [showCom,setShowCom] = useState(true)
  return (
    <div>
      {/* <Count></Count> */}
      {/* <LearnUseState></LearnUseState> */}
      {/* <ImageHideShow></ImageHideShow> */}
      {/* <LearnUseState2></LearnUseState2> */}
      {/* <LearnUseEffect></LearnUseEffect> */}
      {/* <LearnCleanUpFun></LearnCleanUpFun> */}
      {/* <Toggle></Toggle> */}
      {/* <AssignmentTernary></AssignmentTernary> */}
      {/* <Details name="ram" age={24} address = "sankhamul"></Details> */}
      {/* <Info name="Ram" age={25} fatherDetail = {{fname:"kishor",fage:65}} favFood={[" mutton"," chicken"," spinach"]}></Info> */}
      {/* {showCom?<LearnCleanUpFun></LearnCleanUpFun>:null}
      <button onClick={(e)=>{
        setShowCom(true)
      }}>show</button>
      <button onClick={(e)=>{
        setShowCom(false)
      }}>hide</button> */}
      {/* <MyLinks></MyLinks>
      <MyRoutes></MyRoutes> */}
      {/* <Nesting></Nesting> */}
      {/* <Form1></Form1> */}
      {/* <FormikForm></FormikForm> */}
      <FormikTutorial></FormikTutorial>
      
    </div>

  )
  
}

export default App;