import { Field } from 'formik'
import React from 'react'

const FormikRadio = ({name,label,onChange,required,options,...props}) => {
  return (
    <div>
        <Field name={name}>
            {
                ({field,form,meta})=>{
                    return(
                        <div>
                            <label htmlFor={name}>{label}: {required?<span style={{color:"red"}}>*</span>:null}</label>
                            
                                {options.map((item,i)=>{
                                    return(
                                    <div key={i}> 
                                    <label htmlFor={item.value}>{item.label}</label>
                                    
                                    <input 
                                    {...field}
                                    {...props}
                                    type='radio' 
                                    value={item.value} 
                                    onChange={onChange?onChange:field.onChange}
                                    id={meta.value} 
                                    // value={value.value} 
                                    checked={meta.value === item.value} 
                                    // onChange={(e)=>{setGender(e.target.value)}}
                                    ></input>
                                    {meta.touched&&meta.error?<div style={{color:"red"}}>{meta.error}</div>:null}
                                     </div>)
                                })}

                        </div>
                        
                    )
                }
            }
        </Field>
    </div>
  )
}

export default FormikRadio