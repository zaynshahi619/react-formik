import { Field, Form, Formik } from 'formik'
import * as yup from 'yup';
import React from 'react'
import FormikInput from './FormikInput';
import FormikTextArea from './FormikTextArea';
import FormikSelect from './FormikSelect';
import FormikRadio from './FormikRadio';
import FormikCheckBox from './FormikCheckBox';

const FormikForm = () => {
    let initialValues = {
        firstName:"",
        lastName:"",
        description:"",
        days:"",
        gender:"",
        isMarried:false
    }
    let genders = [
        {label:"Male", value:"male"},
        {label:"Female", value:"female"},
        {label:"Other", value:"other"},
    
      ]

    let onSubmit = (value,other)=>{
        console.log(value);
    }
    let validationSchema = yup.object({
        firstName:yup.string().required("FirstName is required"),
        lastName:yup.string().required("Last Name is required"),
        email:yup.string().required("Email is required")
    })
  return (
    <div>
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            {
                (formik)=>{
                    return (
                        <Form>

                            <FormikInput name="firstName" label="First Name" type="text" 
                            // style={{backgroundColor:"red"}}
                            required={true}
                            onChange={(e)=>{
                                formik.setFieldValue("firstName",e.target.value)
                            }}></FormikInput>

                            <FormikInput name="lastName" label="Last Name" required={true}type="text"></FormikInput>

                            <FormikTextArea name="description" label="Description" required={true} type="text"></FormikTextArea>

                            <FormikSelect
                            name="country"
                            label="Country"
                            onChange={(e)=>{
                                formik.setFieldValue("country",e.target.value)
                            }}
                            required={true}
                            option={[
                                {
                                  label: "Sunday",
                                  value: "day1",
                                },
                                {
                                  label: "Monday",
                                  value: "day2",
                                },
                                {
                                  label: "Tuesday",
                                  value: "day3",
                                },
                                {
                                  label: "Wednesday",
                                  value: "day4",
                                },
                                {
                                  label: "Thursday",
                                  value: "day5",
                                },
                                {
                                  label: "Friday",
                                  value: "day6",
                                },
                                {
                                  label: "Saturday",
                                  value: "day7",
                                },
                              ]}
                            >

                            </FormikSelect>
                            <FormikRadio
                            name="gender"
                            label="Gender"
                            onChange={(e)=>{
                                formik.setFieldValue("gender",e.target.value)
                            }}
                            required={true}
                            option={genders}
                            ></FormikRadio>


                            <FormikCheckBox
                            name="isMarried"
                            label="Is Married"
                            onChange={(e)=>{
                                formik.setFieldValue("isMarried",e.target.checked)
                            }}
                            
                            
                            ></FormikCheckBox>


                            {/* <Field name="firstName">
                                {
                                    ({field,form,meta})=>{
                                        return(
                                            <div>
                                                <label htmlFor='firstName'>FirstName:</label>
                                                <input {...field} type='text' placeholder='eg.Ram' value={meta.value}
                                                onChange={field.onChange}
                                                //  onChange={(e)=>{formik.setFieldValue("firstName",e.target.value)}} 
                                                 id='firstName'></input>
                                                {meta.touched&&meta.error?<div style={{color:"red"}}>{meta.error}</div>:null}
                                            </div>
                                            
                                        )
                                    }
                                }
                            </Field>
                            <Field name="lastName">
                                {
                                    ({field,form,meta})=>{
                                        return(
                                            <div>
                                                <label htmlFor='lastName'>lastName:</label>
                                                <input {...field} type='text' placeholder='eg.sharma' value={meta.value} 
                                                onChange={field.onChange}
                                                // onChange={(e)=>{formik.setFieldValue("lastName",e.target.value)}} 
                                                id='lastName'></input>
                                                {meta.touched&&meta.error?<div style={{color:"red"}}>{meta.error}</div>:null}
                                            </div>
                                        )
                                    }
                                }
                            </Field>
                            <Field name="email">
                                {
                                    ({field,form,meta})=>{
                                        return(
                                            <div>
                                                <label htmlFor='email'>Email:</label>
                                                <input {...field} type='text' placeholder='eg.abc@gmail.com' value={meta.value} 
                                                onChange={field.onChange}
                                                // onChange={(e)=>{formik.setFieldValue("email",e.target.value)}} 
                                                id='email'></input>
                                                {meta.touched&&meta.error?<div style={{color:"red"}}>{meta.error}</div>:null}
                                            </div>
                                        )
                                    }
                                }
                            </Field> */}

                            <button type='submit'>Submit</button>
                        </Form>
                    )
                }
            }
        </Formik>

    </div>
  )
}

export default FormikForm