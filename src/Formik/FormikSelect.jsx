import { Field } from 'formik'
import React from 'react'

const FormikSelect = ({name,label,onChange,required,options,...props}) => {
  return (
    <div>
        <Field name={name}>
            {
                ({field,form,meta})=>{
                    return(
                        <div>
                            <label htmlFor={name}>{label}:</label>
                            <select 
                            {...field}
                            {...props}
                            value={meta.value} 
                            onChange={onChange?onChange:field.onChange}>
                                {options.map((item,i)=>{
                                        return (<option key={i} value={item.value}>{item.label}</option>)
                                 })}
                            </select>
                        </div>
                        
                    )
                }
            }
        </Field>
    </div>
  )
}

export default FormikSelect