import { Form, Formik } from 'formik'
import React from 'react'
import * as yup from 'yup';
import FormikInput from './FormikInput';
import FormikRadio from './FormikRadio';
import FormikCheckBox from './FormikCheckBox';
import FormikSelect from './FormikSelect';
import FormikTextArea from './FormikTextArea';


const FormikTutorial = () => {
    let initialValues = {
        fullName:"",
        email:"",
        password:"",
        gender:"",
        country:"",
        isMarried:false,
        description:"",
        phoneNumber:"",
        age:0

    }
    let onSubmit = (value,other)=>{
        console.log(value);
    }
    let validationSchema = yup.object({
        fullName:yup.string().required("FirstName is required")
        .min(4,"Must be atleat 4 character")
        .max(10,"Upto 10 character only")
        .matches(/^[a-zA-Z\s]+$/,"Only alphabet and space are allowed")
        ,
        email:yup.string().required("email is required")
        .matches(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,"email is not valid"),
        password:yup.string().required("password is required")
        .matches(/[!@#$%^&*()_+\-=[\]{};':"|,.<>/?]/,"1 special char, one number,one uppercase min 8 max 10 char"),
        gender:yup.string().required("gender is required"),
        country:yup.string().required("country is required"),
        isMarried:yup.boolean(),
        description:yup.string(),
        phoneNumber:yup.string().required("Phone Number is required")
        .min(8,"min 8 num")
        .max(10,"max 10 num")
        .matches(/^[0-9]+$/,"only number"),
        age:yup.number().required("age Number is required")
        .min(17,"Age must be greater than 17")
        
    })
    let genderOptions = [
        {label:"Male", value:"male"},
        {label:"Female", value:"female"},
        {label:"Other", value:"other"},
    
      ]

    let daysOptions=[
        {
          label: "Sunday",
          value: "day1",
        },
        {
          label: "Monday",
          value: "day2",
        },
        {
          label: "Tuesday",
          value: "day3",
        },
        {
          label: "Wednesday",
          value: "day4",
        },
        {
          label: "Thursday",
          value: "day5",
        },
        {
          label: "Friday",
          value: "day6",
        },
        {
          label: "Saturday",
          value: "day7",
        },
      ]
  return (
    <div>
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            {
                (formik)=>{
                    //for formik we use
                    //name
                    //label
                    //type
                    //onChange
                    //required
                    //option (radio,select)
                    return (
                        <Form>

                           <FormikInput 
                           name="fullName"
                           label="Full Name"
                           type="text"
                           onChange={(e)=>{
                            formik.setFieldValue("fullName",e.target.value)
                           }}
                           required={true}
                           >
                           </FormikInput>

                           <FormikInput
                           name="email"
                           label="Email"
                           type="email"
                           onChange={(e)=>{
                            formik.setFieldValue("email",e.target.value)
                           }}
                           required={true}
                           >
                           </FormikInput>

                           <FormikInput
                           name="password"
                           label="Password"
                           type="password"
                           onChange={(e)=>{
                            formik.setFieldValue("password",e.target.value)
                           }}
                           required={true}
                           >
                           </FormikInput>

                           <FormikRadio
                           name="gender"
                           label="Gender"
                           onChange={(e)=>{
                            formik.setFieldValue("gender",e.target.value)
                           }}
                           required={true}
                           options={genderOptions}
                           >
                           </FormikRadio>

                           <FormikSelect
                           name="days"
                           label="Days"
                           onChange={(e)=>{
                            formik.setFieldValue("days",e.target.value)
                           }}
                           required={true}
                           options={daysOptions}
                           >
                           </FormikSelect>

                           <FormikCheckBox
                           name="isMarried"
                           label="Is Married"
                           onChange={(e)=>{
                            formik.setFieldValue("isMarried",e.target.checked)
                           }}
                           >
                           </FormikCheckBox>

                           <FormikTextArea
                           name="description"
                           label="Description"
                           onChange={(e)=>{
                            formik.setFieldValue("isMarried",e.target.value)
                           }}
                           >
                           </FormikTextArea>

                           <FormikInput
                           name="phoneNumber"
                           label="Phone Number"
                           type="text"
                           onChange={(e)=>{
                            formik.setFieldValue("phoneNumber",e.target.value)
                           }}
                           required={true}
                           >
                           </FormikInput>

                           <FormikInput
                           name="age"
                           label="Age"
                           type="age"
                           onChange={(e)=>{
                            formik.setFieldValue("age",e.target.value)
                           }}
                           required={true}
                           >
                           </FormikInput>



                            <button type='submit'>Submit</button>
                        </Form>
                    )
                }
            }
        </Formik>

    </div>
  )
}

export default FormikTutorial